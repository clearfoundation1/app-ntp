# NTP Server

## Description

The NTP Server app provides the network time protocol service for your systems.  Computers and Internet devices can synchronize their clocks against this server to achieve a  high degree of time accuracy.

- Category: Network
- Subcategory: Infrastructure

## App and API Licenses

- API: LGPLv3
- Frontend: GPLv3

## Powered By

- [Network Time Foundation](https://www.nwtime.org)
